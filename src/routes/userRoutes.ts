import express, { Router, request, response } from "express";
import { UserController } from "../controllers/userController";

export class UserRoutes {
    public router: Router;
    private userController: UserController;
    constructor() {
        this.router = express.Router();
        this.userController = new UserController();
        this.initRoutes();
    }

    private initRoutes(): void {
        this.router.get('/', this.userController.getAllUsers.bind(this.userController));
    }
}
