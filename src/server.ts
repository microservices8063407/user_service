// src/server.ts
import express, { Application } from 'express';
import bodyParser from 'body-parser';
import { UserController } from './controllers/userController';
import dotenv from 'dotenv'
import { UserRoutes } from './routes/userRoutes';

class Server {
  private app: Application;
  public userRoutes: UserRoutes;
  constructor() {
    this.app = express();
    this.userRoutes = new UserRoutes();
    this.config();
    this.routes();
  }

  private config(): void {
    this.app.use(bodyParser.json());
    dotenv.config();
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes(): void {
    
    this.app.use('/api/users',this.userRoutes.router);
  }

  public start(): void {
    const port = process.env.PORT;
    this.app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  }
}

const server = new Server();
server.start();
