// src/controllers/userController.ts
import { Request, Response } from 'express';

export class UserController {
    public getAllUsers(req: Request, res: Response): void {
        res.status(200).json({ message: 'Get all users' });
    }

}